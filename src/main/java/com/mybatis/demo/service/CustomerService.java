package com.mybatis.demo.service;

import com.mybatis.demo.dao.CustomerDAO;
import com.mybatis.demo.model.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CustomerService {
    private static Logger LOGGER = LoggerFactory.getLogger(CustomerService.class);
    @Autowired
    private CustomerDAO customerDAO;

    public List<Customer> getAllCustomers() {
        List<Customer> customers = new ArrayList<>();
        try {
            customers = customerDAO.getAllCustomers();
        } catch(Exception ex) {
            LOGGER.error("Error occurred while getting the customer details :: {} ", ex.getCause());
        }
        return customers;
    }
}
