package com.mybatis.demo.controller;

import com.mybatis.demo.MyBatisDemoApplication;
import com.mybatis.demo.dao.CustomerDAO;
import com.mybatis.demo.model.Customer;
import com.mybatis.demo.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/mybatis")
public class CustomerController {
    private static Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);
    @Autowired
    private CustomerService customerService;
    @GetMapping(value = "/customers")
    private ResponseEntity<List<Customer>> getAllCustomers() {
        LOGGER.info("Entry into getAllCustomers");
        List<Customer> customers = customerService.getAllCustomers();
        return new ResponseEntity<>(customers, HttpStatus.ACCEPTED);
    }
}
