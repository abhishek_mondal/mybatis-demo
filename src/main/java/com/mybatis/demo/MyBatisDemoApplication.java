package com.mybatis.demo;

import com.mybatis.demo.dao.CustomerDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.mybatis.demo")
public class MyBatisDemoApplication {

	private static Logger LOGGER = LoggerFactory.getLogger(MyBatisDemoApplication.class);


	public static void main(String[] args) {

		SpringApplication.run(MyBatisDemoApplication.class, args);
	}

}
