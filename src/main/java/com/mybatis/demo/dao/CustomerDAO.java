package com.mybatis.demo.dao;

import com.mybatis.demo.model.Customer;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CustomerDAO {
    public void saveCustomer(Customer customer);
    public void updateCustomer(Customer customer);
    public void deleteCustomer(String id);
    public List<Customer> getAllCustomers();
}
