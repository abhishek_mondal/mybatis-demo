package com.mybatis.demo.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import javax.annotation.PostConstruct;

@Configuration
@ImportResource(value = "classpath:jdbc-config/jdbc-context.xml")
public class MyBatisConfig {
    private Logger LOGGER = LoggerFactory.getLogger(MyBatisConfig.class);
    @PostConstruct
    public void postMessage() {
        LOGGER.info("Initialization successful...");
    }
}
